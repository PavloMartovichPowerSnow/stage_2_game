import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
    const {name} = fighter;
    const bodyElement = name;
    const title = "Winner:";
    showModal({title, bodyElement});
}
