import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  const firstFighterIndicator = document.getElementById('left-fighter-indicator');
  const secondFighterIndicator = document.getElementById('right-fighter-indicator');
  const healthOfFirstFighter = firstFighter['health'];
  const healthOfSecondFighter = secondFighter['health'];
  const keyArray = [];

  let timeFirstFighter = 0;
  let timeSecondFighter = 0;

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
   
      document.addEventListener('keydown', action => {

        if(!keyArray.includes(action.code)){
          keyArray.push(action.code);
        }

        if(keyArray.includes(controls.PlayerOneAttack) &&
          !keyArray.includes(controls.PlayerTwoBlock) &&
          !keyArray.includes(controls.PlayerOneBlock)){
          secondFighter['health'] -= getDamage(firstFighter, secondFighter);
        } 
        if(keyArray.includes(controls.PlayerTwoAttack) &&
          !keyArray.includes(controls.PlayerOneBlock) &&
          !keyArray.includes(controls.PlayerTwoBlock)){
          firstFighter['health'] -= getDamage(secondFighter, firstFighter);
        }
        
        if(keyArray.includes(...controls.PlayerOneCriticalHitCombination) &&
           Date.now() - timeFirstFighter > 10000) {

           keyArray.forEach(key => {
            if(key === controls.PlayerTwoBlock)
            {
              keyArray.splice(keyArray.indexOf(key), 1);
            }
          });

          secondFighter['health'] -= firstFighter['attack'] * 2;
          timeFirstFighter = Date.now();
        }

        if(keyArray.includes(...controls.PlayerTwoCriticalHitCombination) &&
           Date.now() - timeSecondFighter > 10000) {

           keyArray.forEach(key => {
            if(key === controls.PlayerOneBlock){
              keyArray.splice(keyArray.indexOf(key), 1);
            }
          });

          firstFighter['health'] -= secondFighter['attack'] * 2;
          timeSecondFighter = Date.now();
        }

        firstFighterIndicator.style.width = (firstFighter['health']/healthOfFirstFighter) * 100 + '%';
        secondFighterIndicator.style.width = (secondFighter['health']/healthOfSecondFighter) * 100 + '%';

        if(firstFighter['health'] <= 0){
          resolve(secondFighter);
        }
        if(secondFighter['health'] <= 0){
          resolve(firstFighter);
        }

      });

      document.addEventListener('keyup', action => {
        keyArray.splice(keyArray.indexOf(action.code));
      });

  });
  
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage >= 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const randomNumber = Math.random() * 2;
  return fighter['attack'] * randomNumber;
}

export function getBlockPower(fighter) {
  // return block power
  const randomNumber = Math.random();
  return fighter['defense'] * randomNumber;
}
