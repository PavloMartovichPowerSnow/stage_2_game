import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const paramethers = ['name', 'health', 'attack', 'defense'];
  let discription;
  if(fighter){
    fighterElement.appendChild(createFighterImage(fighter));
    for(let i of paramethers){
      discription = document.createElement('p');
      discription.innerHTML = i + ': ' + fighter[i];
      fighterElement.appendChild(discription);
    }
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
